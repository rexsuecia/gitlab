#!/bin/bash -e

function export_build_info {
    CI_JOB_ID=${CI_JOB_ID}
    BUILD_ID=${CI_JOB_ID:-"local"}

    # We actually want to grep for * here
    # shellcheck disable=2063
    GIT_BRANCH=$(git branch | grep '*' | cut -d ' ' -f2)
    GIT_BRANCH=${GIT_BRANCH#remotes/origin/}

    SOURCE_VERSION=$(git rev-parse HEAD)
    SOURCE_VERSION=${SOURCE_VERSION:0:10}

    PACKAGE_VERSION=$(jq .version ./package.json | tr -d \")

    CURRENT_BUILD_DATE=$(date --iso-8601=seconds)

    export BUILD_ID
    export GIT_BRANCH
    export SOURCE_VERSION
    export PACKAGE_VERSION
    cat <<EOF > ./version.json
{
  "build" : "${BUILD_ID}",
  "branch" : "${GIT_BRANCH}",
  "commit" : "${SOURCE_VERSION}",
  "version" : "${PACKAGE_VERSION}",
  "time" : "${CURRENT_BUILD_DATE}"
}
EOF
}

export_build_info

