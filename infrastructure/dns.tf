data "aws_route53_zone" "rexsuecia-com" {
  provider     = "aws.dns"
  name         = var.dns_zone
  private_zone = false
}

# The DNS Name that routes to the API
resource "aws_route53_record" "api" {
  provider = "aws.dns"
  name     = aws_api_gateway_domain_name.api.domain_name
  type     = "A"
  zone_id  = data.aws_route53_zone.rexsuecia-com.id

  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.api.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.api.cloudfront_zone_id
  }
}
