data "aws_iam_policy_document" "api-method-credentials" {
  statement {
    actions = [
      "lambda:InvokeFunction",
    ]

    resources = [
      "*",
    ]
  }
}

data "aws_iam_policy_document" "api-trust" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      identifiers = [
        "apigateway.amazonaws.com",
      ]

      type = "Service"
    }
  }
}

resource "aws_iam_role" "api-execution" {
  assume_role_policy = data.aws_iam_policy_document.api-trust.json
  name               = "API_GW_GITLAB_EXECUTE_LAMBDA"
  description        = "Allows API gw to invoke any Lambda function. Simplifies greatly"
}

resource "aws_iam_policy" "api-policy" {
  policy      = data.aws_iam_policy_document.api-method-credentials.json
  name        = "API_GW_GITLAB_EXECUTE_LAMBDA"
  description = "Allows API GW to invoke all lambda functions for Gitlab"
}

resource "aws_iam_role_policy_attachment" "api" {
  policy_arn = aws_iam_policy.api-policy.arn
  role       = aws_iam_role.api-execution.name
}
