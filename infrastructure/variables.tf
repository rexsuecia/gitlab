variable "aws_profile" {
  description = "The AWS profile to use"
  default     = "default"
}

variable "region" {
  description = "The main region to use"
  default     = "eu-west-1"
}

variable "dns_zone" {
  default = "dartistheshit.pub"
}

variable "subdomain" {
  description = "The subdomain to use for this API"
}

variable "account_number" {
  description = "The account number to use e.g. 285017407485"
}

variable "cert_region" {
  default = "us-east-1"
}

variable "assume_role" {
  description = "The role to assume to apply resources. Defined in aws-iac"
}

variable "dns_assume_role" {
  description = "A role to assume to manage DNS on Group"
  default     = "arn:aws:iam::285017407485:role/DEVELOPER_DNS_ROLE"
}

variable "white_list_users" {
  description = "Tells if users should be blocked if not on white list"
  default     = "FALSE"
}
