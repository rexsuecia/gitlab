terraform {
  backend "s3" {
    key            = "rexsuecia/gitlab/terraform.tfstate"
    bucket         = "rexsuecia-terraform-states"
    dynamodb_table = "rexsuecia-terraform-statelock"
    region         = "eu-west-1"
    profile        = "rexsuecia"
    role_arn       = "arn:aws:iam::285017407485:role/DEVELOPER_TERRAFORM_ROLE"
    encrypt        = true
    acl            = "bucket-owner-full-control"
  }
  required_version = "~> 0.12"
}

provider "aws" {
  region  = var.region
  profile = var.aws_profile

  assume_role {
    role_arn = var.assume_role
  }

  version = "~> 1"
}

provider "aws" {
  alias   = "cert"
  region  = var.cert_region
  profile = var.aws_profile

  assume_role {
    role_arn = var.assume_role
  }

  version = "~> 1"
}

# The DNS, Route53 tend to be managed elsewhere
provider "aws" {
  alias   = "dns"
  region  = var.region
  profile = var.aws_profile

  assume_role {
    role_arn = var.dns_assume_role
  }

  version = "~> 1"
}

provider "template" {
  version = "~> 1"
}

provider "archive" {
  version = "~> 1"
}

provider "local" {
  version = "~> 1"
}
