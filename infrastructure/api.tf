resource "aws_api_gateway_domain_name" "api" {
  certificate_arn = aws_acm_certificate.api_cert.arn
  domain_name     = "${var.subdomain}.${var.dns_zone}"
}

# This is because there is a "conflict" between the YAML description
# and the description on the terraform reource, oh so silly.
locals {
  description = "Gitlab API"
  title       = "Gitlab API"
}

data "template_file" "open_api" {
  vars = {
    account_number = var.account_number
    host_name      = "${var.subdomain}.${var.dns_zone}"
    execution_role = aws_iam_role.api-execution.arn
    region         = var.region
    description    = local.description
    title          = local.title
  }

  template = file("../spec/spec.yml")
}

# This is needed to get a change detected when the Yaml is updated.
# A bit of a hack.
data "archive_file" "test" {
  type        = "zip"
  source_file = "../spec/spec.yml"
  output_path = "spec.zip"
}

resource "aws_api_gateway_rest_api" "api" {
  name = local.title

  description = local.description

  body = data.template_file.open_api.rendered

  endpoint_configuration {
    types = [
      "REGIONAL",
    ]
  }
}

resource "aws_api_gateway_stage" "api" {
  deployment_id = aws_api_gateway_deployment.deployment.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
  stage_name    = "LIVE"

  xray_tracing_enabled = true

  variables = {
    # The VERSION is to trigger updates when the YAML changes.
    WHITE_LIST_USERS = var.white_list_users
    VERSION          = data.archive_file.test.output_sha
  }
}


resource "aws_api_gateway_deployment" "deployment" {
  rest_api_id = aws_api_gateway_rest_api.api.id

  variables = {
    # The VERSION is to trigger updates when the YAML changes.
    VERSION = data.archive_file.test.output_sha

  }

  # Needed so that the API base path mapping is not breaking. Horray!
  lifecycle {
    create_before_destroy = true
  }
}


# This let us access the API on a friendly domain.
resource "aws_api_gateway_base_path_mapping" "mapping" {
  api_id      = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_stage.api.stage_name
  domain_name = "${var.subdomain}.${var.dns_zone}"
  base_path   = ""

  # Routes all requests to the only stage. Crazy enough "/" does not work
}
