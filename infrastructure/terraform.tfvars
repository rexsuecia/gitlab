aws_profile = "rexsuecia"

region = "eu-west-1"

assume_role = "arn:aws:iam::285017407485:role/DEVELOPER_ROLE"

subdomain = "gitlab-api"

dns_zone = "rexsuecia.com"

cert_region = "us-east-1"

account_number = "285017407485"
