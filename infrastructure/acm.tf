resource "aws_acm_certificate" "api_cert" {
  provider          = "aws.cert"
  domain_name       = "${var.subdomain}.${var.dns_zone}"
  validation_method = "DNS"
}

resource "aws_route53_record" "api_validation" {
  provider = "aws.dns"
  name     = lookup(aws_acm_certificate.api_cert.domain_validation_options[0], "resource_record_name")
  type     = lookup(aws_acm_certificate.api_cert.domain_validation_options[0], "resource_record_type")
  zone_id  = data.aws_route53_zone.rexsuecia-com.id

  records = [
    lookup(aws_acm_certificate.api_cert.domain_validation_options[0], "resource_record_value"),
  ]

  ttl = 60
}
