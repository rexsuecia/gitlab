const https = require('https')

const ACCESS_TOKEN = process.env.ACCESS_TOKEN
const PROJECT_ID = process.env.PROJECT_ID || '12652782'

const getFromHttps = (HTTPS_OPTIONS, data) => {
  return new Promise((resolve, reject) => {
    const req = https.request(HTTPS_OPTIONS, (res) => {
      let body = ''
      res.on('data', (chunk) => {
        body += chunk
      })
      res.on('end', () => {
        if (res.statusCode === 204) {
          return resolve({})
        }
        if (body !== '') {
          try {
            const result = JSON.parse(body)
            return resolve(result)
          } catch (e) {
            // This should normally not happen, but do sometimes.
            console.error(e)
            console.error(body)
            console.error(res.headers)
            reject(e)
          }
        }
        // Return the result object if no dataa. Assume the callee wants headers and stuff.
        return resolve(res)
      })
    })

    req.on('error', (e) => {
      console.error(e)
      reject(e)
    })

    if (data) {
      req.write(data)
    }
    req.end()
  })
}

const getBranchByName = async name => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/repository/branches`,
    hostname: 'gitlab.com',
    method: 'GET',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN
    }
  }

  return getFromHttps(HTTPS_OPTIONS).then(res => {
    console.log(res)
    return res.filter(item => {
      return item.name === name
    })
  })
}

const createBranch = (fromName, toName) => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/repository/branches?branch=${toName}&ref=${fromName}`,
    hostname: 'gitlab.com',
    method: 'POST',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN
    }
  }
  return getFromHttps(HTTPS_OPTIONS)
}

const deleteBranch = (name) => {
  if (name === 'master' || name === 'develop') {
    throw new Error(`We do not delete ${name} branch with this script`)
  }
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/repository/branches/${name}`,
    hostname: 'gitlab.com',
    method: 'DELETE',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN
    }
  }
  return getFromHttps(HTTPS_OPTIONS).then(res => {
    console.log(res)
  })
}

const createMR = (from, to) => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/merge_requests`,
    hostname: 'gitlab.com',
    method: 'POST',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN,
      'Content-Type': 'application/json'
    }
  }

  const data = JSON.stringify({
    id: PROJECT_ID,
    source_branch: from,
    target_branch: to,
    title: 'Automated MR'
  })

  return getFromHttps(HTTPS_OPTIONS, data).then(res => {
    console.log(res)
    return res.iid
  })
}

const acceptMR = (id) => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/merge_requests/${id}/merge`,
    hostname: 'gitlab.com',
    method: 'PUT',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN,
      'Content-Type': 'application/json'
    }
  }

  const data = JSON.stringify({
    id: PROJECT_ID,
    merge_request_iid: id,
    merge_when_pipeline_succeeds: true
  })

  return getFromHttps(HTTPS_OPTIONS, data).then(res => {
    console.log(res)
  })
}

const getMR = (id) => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/merge_requests/${id}`,
    hostname: 'gitlab.com',
    method: 'GET',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN,
      'Content-Type': 'application/json'
    }
  }

  return getFromHttps(HTTPS_OPTIONS).then(res => {
    console.log(res)
    return res
  })
}

const cancelJob = async jobid => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/jobs/${jobid}/cancel`,
    hostname: 'gitlab.com',
    method: 'POST',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN
    }
  }
  return getFromHttps(HTTPS_OPTIONS)
    .then(res => {
      return res
    })
}

const getJobs = async () => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/jobs?scope[]=pending&scope[]=running&scope[]=created`,
    hostname: 'gitlab.com',
    method: 'GET',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN
    }
  }
  return getFromHttps(HTTPS_OPTIONS)
    .then(res => {
      return res
    })
}

const getLastCommitId = async (branch) => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/repository/commits?ref_name=${branch}`,
    hostname: 'gitlab.com',
    method: 'GET',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN
    }
  }
  return getFromHttps(HTTPS_OPTIONS)
    .then(res => {
      console.log()
      return res[0].id // We assume that first in list is the latest
    })
}

const getFile = (file, branch) => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/repository/files/${encodeURIComponent(file)}?ref=${branch}`,
    hostname: 'gitlab.com',
    method: 'GET',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN
    }
  }
  return getFromHttps(HTTPS_OPTIONS).then(res => {
    let buff = Buffer.from(res.content, 'base64')
    return buff.toString('utf8')
  })
}

const saveFile = async (fileContents, fileName, branch) => {
  const HTTPS_OPTIONS = {
    path: `/api/v4/projects/${PROJECT_ID}/repository/files/${encodeURIComponent(fileName)}`,
    hostname: 'gitlab.com',
    method: 'PUT',
    headers: {
      'PRIVATE-TOKEN': ACCESS_TOKEN,
      'Content-Type': 'application/json'
    }
  }
  let buff = Buffer.from(JSON.stringify(fileContents, null, 2) + '\n', 'utf8').toString('base64')
  console.log(buff)
  const data = JSON.stringify({
    file_path: encodeURIComponent(fileName),
    branch: branch,
    encoding: 'base64',
    content: buff,
    commit_message: 'Bot Update of file'
  })
  return getFromHttps(HTTPS_OPTIONS, data)
    .then(res => {
      // console.log(res)
      return res
    })
}

const getPackageJson = branch => {
  return getFile('package.json', branch)
    .then(file => {
      // console.log(file)
      return JSON.parse(file)
    })
}

const updateVersion = async (file, level) => {
  let parts = file.version.split('.')
  let pos = 0 // Major
  if (level === 'MAJOR') {
    parts[1] = 0
    parts[2] = 0
  } else if (level.toUpperCase() === 'MINOR') {
    pos = 1
    parts[2] = 0
  } else if (level.toUpperCase() === 'PATCH') {
    pos = 2
  } else {
    throw new Error('Not a valid level, use MAJOR|MINOR|PATCH')
  }
  parts[pos] = Number.parseInt(parts[pos]) + 1
  file.version = parts.join('.')
  return file
}

const stageVersion = async (level) => {
  let TARGET_BRANCH
  let COMMIT
  let FILE

  return getPackageJson('develop')
    .then((file) => {
      return updateVersion(file, level.toUpperCase()).then(file => {
        FILE = file
        // console.log(res)
        TARGET_BRANCH = 'release_v' + file.version
        return createBranch('develop', TARGET_BRANCH)
      }).then(() => {
        return saveFile(FILE, 'package.json', TARGET_BRANCH)
      }).then(() => {
        return getLastCommitId(TARGET_BRANCH)
      }).then(res => {
        COMMIT = res
        return getJobs(TARGET_BRANCH)
      }).then(res => {
        console.log(COMMIT)
        return Promise.all(
          res.map(job => {
            console.log('commit: ' + job.commit.id)
            if (job.commit.id !== COMMIT) {
              return cancelJob(job.id)
            }
          })
        )
      })
    })
}

const releaseVersion = async version => {
  let MR_ID
  return createMR('release_v' + version, 'master')
    .then((id) => {
      MR_ID = id
      return acceptMR(MR_ID)
    }).then(() => {
      return deleteBranch('release_v' + version)
    }).then(() => {
      return createMR('master', 'develop')
    }).then(id => {
      return acceptMR(id)
    })
}
if (process.argv[2] === 'STAGE') {
  stageVersion(process.argv[3]).then(res => {
    console.log(res)
  })
} else if (process.argv[2] === 'RELEASE') {
  releaseVersion(process.argv[3])
    .then(res => {
      console.log(res)
    })
}

module.exports = {
  acceptMR,
  getMR,
  getBranchByName,
  deleteBranch,
  createMR,
  createBranch,
  saveFile
}
